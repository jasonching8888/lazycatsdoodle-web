$(function () {
  function inc() {
    let val = parseInt($("#mint-txt").val());
    val = Math.min(10, val + 1);

    $("#mint-txt").val(val);
  }

  function minus() {
    let val = parseInt($("#mint-txt").val());
    val = Math.max(1, val - 1);
    $("#mint-txt").val(val);
  }

  $("#mint-btn-plus").click(() => {
    inc();
  });

  $("#mint-btn-minus").click(() => {
    minus();
  });
});
